<?php 

namespace App\Core;

Class App {
	protected static $registry = [];

	public static function bind($key, $value){
		static::$registry[$key] = $value;
	}

	public static function get($key){
		if (! array_key_exists($key, static::$registry)) {
			throw Exception("No existe dependencia {$key}");
		}
		return static::$registry[$key];
	}
}