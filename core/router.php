<?php

namespace App\Core; 

Class Router {

	protected $routes = [
		'GET' => [],
		'POST' => []
	];

	public static function load($file){
		$router = new static;
		require $file;
		return $router;
	}

	public function get($uri, $controller){
		$this->routes['GET'][$uri] = $controller;
	}

	public function post($uri, $controller){
		$this->routes['POST'][$uri] = $controller;
	}

	public function direct($uri, $method){
		if (array_key_exists($uri, $this->routes[$method])) {
			$call = explode('@', $this->routes[$method][$uri]);
			return $this->callAction(
				$call[0],
				$call[1]
			);
		}

		throw new Exception('No existe la ruta');
	}

	protected function callAction($controller, $action){
		$controller = "App\\Controllers\\{$controller}";
		$controller = new $controller;
		if (! method_exists($controller, $action)) {
			throw new Exception("{$controller} no responde a la accion {$action}");
		}

		return $controller->$action();
	}

}