<?php 

$router->get('', 'TodoController@index');
$router->get('about', 'TodoController@about');
$router->get('contact', 'TodoController@contact');
$router->get('todo', 'TodoController@create');
$router->post('task', 'TodoController@store');