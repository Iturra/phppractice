<?php  

namespace App\Controllers;

use App\Core\App;

Class TodoController {

	public function index(){
		$tasks = App::get('database')->selectAll('todos');
		return view('index', ['tasks' => $tasks]);
	}

	public function about(){
		return view('about');
	}

	public function contact(){
		$number = '9-65874589';
		return view('contact', ['number' => $number]);
	}
	
	public function create(){
		return view('create');
	}

	public function store(){
		App::get('database')->insert('todos', [
			'todo' => $_POST['task'],
			'completed' => 0
		]);
		return redirect('');
	}

}